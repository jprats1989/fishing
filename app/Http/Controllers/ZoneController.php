<?php

namespace App\Http\Controllers;

use App\User;
use App\Zone;
use App\Version;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ZoneController extends Controller
{
    public function create()
    {
        return view('zones.create', ['zones' =>  Zone::getVersions(), 'zone_model' => new Version]);
    }

    public function edit($version){
        $version = Version::withoutGlobalScope('validated')->withoutGlobalScope('published')->find($version);
        if(!$version->published){
            $form_route = route('zones.preview');
            $text_button = "Previsualizar";
        }else{
            $form_route = route('zones.new-version');
            $text_button = "Proponer";
        }
        
        return view('zones.edit', ['zone' => $version, 'zone_model' => new Version, 'form_route' => $form_route, 'text_button' => $text_button]);
    }

    public function index()
    {
        return view('zones.index', ['zones' => Zone::getVersions()]);
    }


    /***
        If we come from Create, we create a new Zone and Version.
        If we come from Edit, we will just uptade the Version.
    ***/
    public function preview(Request $request){
        if(null == $request->input('version_id')){
            $zone = auth()->user()->zones()->save(new Zone());
            $zone->save();
            $version = auth()->user()->versions()
                ->save(new Version(array_merge($request->all(), ['zone_id' => $zone->id])));
        }else{
            $version = Version::withoutGlobalScope('validated')
                ->withoutGlobalScope('published')
                ->find($request->input('version_id'));
            $version->update($request->all());

        }

        $version->descriptions()->delete();
        $version->descriptions()->createMany($request->description);
        return view('zones.show', ['zone' => $version]);   
    }

    public function show(Version $version){
        return view('zones.show', ['zone' => $version]);
    }

    public function newVersion(Request $request){
        $suggestion = auth()->user()->versions()->save(new Version($request->all()));
        return view('zones.index', ['zones' => Zone::getVersions()]);
    }

    public function publish($version)
    {
        $version = Version::withoutGlobalScope('validated')->withoutGlobalScope('published')->find($version);
        $version->published = true;
        $version->save();       
        return redirect('zonas/nueva');
    }


}
