<?php

namespace App\Http\Middleware;

use App\Version;
use Closure;
use Illuminate\Support\Facades\Auth;

class isOwnerOrAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // For some routes zone_id is a optional param
        if(null !== $request->route('id')){
            $zone = Version::withoutGlobalScope('validated')
                        ->withoutGlobalScope('published')
                        ->find($request->route('id'));

            if ($zone->user_id != Auth::user()->id)
            {
                return redirect('no-permission');
            }
        }

        return $next($request);
    }
}
