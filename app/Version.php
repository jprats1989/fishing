<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class Version extends Model
{
    protected $table = 'versions';
    protected $fillable = ['type', 'name', 'slug', 'latitude', 'length', 'zone_id'];
    protected $attributes = array('validated' => false, 'published' => false);

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('validated', function (Builder $builder) {
            $builder->where('validated', true);
        });
        static::addGlobalScope('published', function (Builder $builder) {
            $builder->where('published', true);
        });
    }

    public static function getTypes()
    {
        return config('constants.zone_types');
    }
    
    public function getUrlAttribute()
    {
        return route('zones.show', [$this->id, $this->slug]);
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = $value;
        $this->attributes['slug'] = Str::slug($value);
    }

    public function descriptions()
    {
        return $this->hasMany('App\VersionDescription');
    }

    
}
