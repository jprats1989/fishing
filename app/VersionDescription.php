<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VersionDescription extends Model
{
    protected $table = 'versions_descriptions';
    protected $fillable = ['title', 'text'];
}
