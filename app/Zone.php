<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class Zone extends Model
{
    protected $table = 'zones';
    
    protected $attributes = ['active' => true];
    

    /*public static function getZones($user)
    {
        if (!isset($user) || $user->hasRole('user')) {
            return Zone::all();
        } else {
            return Zone::withoutGlobalScope('validated')->get();
        }
    }*/

    public static function getVersions(){
        return  Zone::with('versionActive')->get()->pluck('versionActive')->filter();
    }

    public function getUrlAttribute()
    {
        return route('zones.show', [$this->id, $this->slug]);
    }

    public function versionActive(){
        return $this->hasOne('App\Version')->latest();
    }

    public function versions(){
        return $this->hasMany('App\Version');
    }

    


}
