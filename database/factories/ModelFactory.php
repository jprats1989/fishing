<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});


$factory->define(App\Zone::class, function (Faker\Generator $faker) {
    $zone =  new App\Zone;
    return [
        'user_id' => DB::table('users')->where('id', 3)->first()->id
    ];
});

$factory->define(App\Version::class, function (Faker\Generator $faker) {
    $version =  new App\Version;
    return [
        'type' => $faker->randomElement($version->getTypes()),
        'name' => $faker->city,
        'latitude' => $faker->latitude($min = 37.95, $max = 40.5),
        'length' => $faker->longitude($min = -6.97, $max = -5.13),
        'user_id' => DB::table('users')->where('id', 3)->first()->id,
        'zone_id' => 0
    ];
});


$factory->define(App\VersionDescription::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->sentence(),
        'text' => $faker->text(),
    ];
});
