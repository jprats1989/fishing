<?php

use Illuminate\Database\Seeder;
class ZonesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker\Generator $faker)
    {

        $zones = factory(App\Zone::class, 50)
            ->create(['active' => $faker->boolean(80)]);

        foreach($zones as $zone){
            factory(App\Version::class, 3)
            ->create(['published' => $faker->boolean(50),'validated' =>  $faker->boolean(50), 'zone_id' => $zone->id])
            ->each(function ($version) {
                $version->descriptions()->saveMany(factory(App\VersionDescription::class, 3)->make());
            });
        }
    }
}
