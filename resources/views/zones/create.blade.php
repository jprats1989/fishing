@extends('layouts.app')

@section('content')

        <div class="row">
            <div class="col-md-12">
                <h3>Selecciona la zona que deseas añadir</h3>
                <div id="map"></div>                

                <h3>Completa los campos y guarda</h3>
                {{ Form::open(['url' => route('zones.preview')]) }}
                {{ Form::token() }}
                {{ Form::hidden('latitude', null, array('id' => 'latitude', 'required'))  }}
                {{ Form::hidden('length', null, array('id' => 'length', 'required')) }}

                <div class="form-group col-md-4">
                    {{ Form::label('name', null, ['class' => 'control-label']) }}
                    {{ Form::text('name', null, ['required']) }}
                </div>

                <div class="form-group col-md-4">
                    <div id="descriptions" class="col-md-12">
                            {{ Form::text('description[0][title]', 'Descripción General', ['class' => 'title']) }}
                            {{ Form::textarea('description[0][text]', null, ['rows' => 5]) }}
                    </div>
                    <button type="button" id="new_section">Añadir sección</button>
                </div>
                {!! Form::select('type', $zone_model->getTypes()) !!}
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 pull-right">
                {{ Form::submit('Previsualizar', ['class' => 'btn-primary']) }}
            </div>
        </div>
@endsection

@push('js_includes')
<script>
    $(document).ready(function(){
        var cont = 0;
        $("#new_section").click(function(){
            cont++;
            $("#descriptions").append("<div id='description"+cont+"'><input placeholder='Titulo...' type='text' name='description["+cont+"][title]' class='title'><textarea placeholder='Texto...' name='description["+cont+"][text]' rows='5'></textarea><button type='button' onclick='delete_desc("+cont+")' class='delete-desc btn btn-danger'>X</button></div>");
        });
     });

    function delete_desc(id){
        $("#description" + id).remove();
    }

    function initMap() {
        var pinColor = "1c86bb";
        var pinImage = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + pinColor,
        new google.maps.Size(21, 34),
        new google.maps.Point(0,0),
        new google.maps.Point(10, 34));
        var pinShadow = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_shadow",
        new google.maps.Size(40, 37),
        new google.maps.Point(0, 0),
        new google.maps.Point(12, 35));

        var caceres = {lat: 39.475277,  lng: -6.372425};
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 8,
            center: caceres 
        });

        @foreach($zones as $zone)    
            var latLng = {lat: {{$zone->latitude}}, lng: {{$zone->length}}};
            var marker = new google.maps.Marker({
                position: latLng, 
                map: map,
                icon: pinImage,
                shadow: pinShadow
            });
        @endforeach

        google.maps.event.addListener(map, 'click', function( event ){
            if(typeof(marker) != 'undefined'){
                marker.setMap(null);    
            }      
            marker = new google.maps.Marker({position: event.latLng, map: map});
            $("#latitude").val(event.latLng.lat())
            $("#length").val(event.latLng.lng())
        });
    }
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAAsev39_AsPF5Ri_Eh-yyMHAaqx7B-omg&callback=initMap"></script>
@endpush

