@extends('layouts.app')
{{-- Si un usuario está editando una zona propia antes de publicarla --}}

@section('content')
    <div class="container">
        <div class="row">
            <h3>Selecciona la zona que deseas añadir</h3>
            <div id="map"></div>                

            <h3>Completa los campos y guarda</h3>
            {{ Form::open(['url' => $form_route]) }}
            {{ Form::token() }}
            {{ Form::hidden('latitude', $zone->latitude, array('id' => 'latitude', 'required'))  }}
            {{ Form::hidden('length', $zone->length, array('id' => 'length', 'required')) }}
            {{ Form::hidden('version_id', $zone->id)}}
            {{ Form::hidden('zone_id', $zone->zone_id)}}

            <div class="form-group col-md-4">
                {{ Form::label('name', null, ['class' => 'control-label']) }}
                {{ Form::text('name', $zone->name, ['required']) }}
            </div>

            @for($i=0; $i< count($zone->descriptions); $i++)
                <div class="form-group col-md-4">
                    <div id="descriptions" class="col-md-12">
                        <div id="description{{$i}}">
                            {{ Form::text("description[$i][title]", $zone->descriptions[$i]->title, ['class' => 'title']) }}
                            {{ Form::textarea("description[$i][text]", $zone->descriptions[$i]->text, ['rows' => 5]) }}
                            @if($i!=0)
                                <button type='button' onclick='delete_desc({{$i}})' class='delete-desc btn btn-danger'>X</button>
                            @endif
                        </div>
                    </div>
                </div>
            @endfor
            <button type="button" id="new_section">Añadir sección</button>
            {!! Form::select('type', config('constants.zone_types'), $zone->type) !!}
            {{ Form::submit($text_button) }}
            {{Form::close()}}
        </div>
    </div>
@endsection

@push('js_includes')
<script>

     $(document).ready(function(){
        var cont = {{count($zone->descriptions)}};
        $("#new_section").click(function(){
            $("#descriptions").append("<div id='description"+cont+"'><input placeholder='Titulo...' type='text' name='description["+cont+"][title]' class='title'><textarea placeholder='Texto...' name='description["+cont+"][text]' rows='5'></textarea><button type='button' onclick='delete_desc("+cont+")' class='delete-desc btn btn-danger'>X</button></div>");
            cont++;
        });
     });

    function delete_desc(id){
        $("#description" + id).remove();
    }

    function initMap() {
        var pinColor = "1c86bb";
        var pinImage = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + pinColor,
        new google.maps.Size(21, 34),
        new google.maps.Point(0,0),
        new google.maps.Point(10, 34));
        var pinShadow = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_shadow",
        new google.maps.Size(40, 37),
        new google.maps.Point(0, 0),
        new google.maps.Point(12, 35));

        var latLng = {lat: {{$zone->latitude}}, lng: {{$zone->length}}};
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 8,
            center: latLng 
        });

        var marker = new google.maps.Marker({
            position: latLng, 
            map: map,
            icon: pinImage,
            shadow: pinShadow
        });

        google.maps.event.addListener(map, 'click', function( event ){
            marker = new google.maps.Marker({position: event.latLng, map: map});
            $("#latitude").val(event.latLng.lat())
            $("#length").val(event.latLng.lng())
        });
    }
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAAsev39_AsPF5Ri_Eh-yyMHAaqx7B-omg&callback=initMap"></script>
@endpush

