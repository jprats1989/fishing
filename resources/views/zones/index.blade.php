@extends("layouts.app")
@section("content")
    <div class="col-md-8">    
        <ul>
            @foreach($zones as $zone)
                <li>
                    <a href="{{route('zones.show', [$zone->id, $zone->slug])}}">
                        {{$zone->name}}
                    </a>
                </li>
            @endforeach
        </ul>
    </div>
    <div class="col-md-4">
        <a href="{{route('zones.create')}}">
            <button class="btn-primary pull-right">Crear Zona</button>
        </a>
    </div>
@endsection