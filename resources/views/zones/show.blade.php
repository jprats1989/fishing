@extends('layouts.app')
@push('css_includes')
    <style>
        #map {
            height: 400px;
            width: 100%;
        }
        input, textarea{
            width: 100% !important;
        }
    </style>
@endpush

@section('content')
    <h1>{{ $zone->name }}</h1>
    <div id="map"></div>
    <p>{{ $zone->type }}</p>
    <p>{{ $zone->latitude }} - {{ $zone->length }}</p>
    @foreach($zone->descriptions as $description)
        <h4>{{$description['title']}}</h4>
        <p>{{$description['text']}}</p>
    @endforeach

    {{--  Estamos previsualizando  --}}
    @if(!$zone->published)
        <a href="{{route('zones.edit', [$zone->id, $zone->slug])}}"><button>Atras</button></a>
        <a href="{{route('zones.publish', [$zone->id])}}"><button>Publicar</button></a>
    @else
        <a href="{{route('zones.edit', [$zone->id, $zone->slug])}}"><button>Proponer un cambio / Añadir contenido</button></a>
    @endif
@endsection


@push('js_includes')

<script>

    function initMap() {
        var pinColor = "1c86bb";
        var pinImage = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + pinColor,
        new google.maps.Size(21, 34),
        new google.maps.Point(0,0),
        new google.maps.Point(10, 34));
        var pinShadow = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_shadow",
        new google.maps.Size(40, 37),
        new google.maps.Point(0, 0),
        new google.maps.Point(12, 35));

        var caceres = {lat: 39.475277,  lng: -6.372425};
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 8,
            center: caceres 
        });
 
        var latLng = {lat: {{$zone->latitude}}, lng: {{$zone->length}}};
        var marker = new google.maps.Marker({
            position: latLng, 
            map: map,
            icon: pinImage,
            shadow: pinShadow
        });
    }
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAAsev39_AsPF5Ri_Eh-yyMHAaqx7B-omg&callback=initMap"></script>

@endpush