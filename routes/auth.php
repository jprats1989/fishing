<?php

use Illuminate\Http\Request;
Route::prefix('zonas')->group(function () {
    Route::get('nueva/{id?}', 'ZoneController@create')->name('zones.create');
    Route::post('previsualizar', 'ZoneController@preview')->name('zones.preview');
    Route::get('publicar/{id}', 'ZoneController@publish')->name('zones.publish')->middleware('isOwnerOrAdmin');
    Route::get('editar-zona/{version}-{slug}', 'ZoneController@edit')->name('zones.edit');
    Route::get('sugerir-cambio/{version}-{slug}', 'ZoneController@createSuggestion')->name('zones.createSuggestion');
    Route::post('sugerir-cambio', 'ZoneController@newVersion')->name('zones.new-version');
    // https://www.opsou.com/es/blog/entidad-con-propietario-en-laravel-54
});

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');