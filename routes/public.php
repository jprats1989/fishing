<?php

use Illuminate\Http\Request;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/no-permission', 'Controller@isNotAuthorized');

Route::prefix('zonas')->group(function () {
    Route::get('/', 'ZoneController@index')->name('zones.index');
    Route::get('/{version}-{slug}', 'ZoneController@show')->name('zones.show');
});

