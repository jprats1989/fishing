<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\User;


class ZoneTest extends DuskTestCase
{
    
    public function test_user_can_create_a_new_unvalidated_zone(){

        $this->browse(function (Browser $browser) {
            
            // Having
            $user = factory(User::class)->create()->attachRole('user');            
            
            $browser->loginAs($user)
                ->visit(route('zones.create'));

            // When
            $browser->click('#map')
                ->type('name', 'Este es el titulo')
                ->type('description[0][text]', 'Esta es la descripcion')
                ->press('Previsualizar')
                ->press('Publicar');

            // Then    
            $this->assertDatabaseHas('versions', ['name' => 'Este es el titulo', 'validated' => false, 'published' => true]);
        });

            
    }

    public function test_several_description_sections_can_be_created(){
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create()->attachRole('user');

            $browser->loginAs($user)
                 ->visit(route('zones.create'))
                 ->click('#map')
                 ->type('name', 'Este es el título')
                 ->type('description[0][title]', 'Descripción General')
                 ->type('description[0][text]', 'Esta es la descripcion general')
                 ->press('Añadir sección')
                 ->type('description[1][title]', 'Seccion 2')
                 ->type('description[1][text]', 'Esta es la seccion 2')
                 ->press('Previsualizar');

             $this->assertDatabaseHas('versions_descriptions', ['title' => 'Seccion 2', 'text' => 'Esta es la seccion 2']);
         });
    }

    public function test_user_can_create_preview_edit_preview_create(){
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();
            $user->attachRole('user');

            $browser->loginAs($user)
                 ->visit(route('zones.create'))
                 ->click('#map')
                 ->type('name', 'Este es el título')
                 ->type('description[0][title]', 'Descripción General')
                 ->type('description[0][text]', 'Esta es la descripcion general')
                 ->press('Añadir sección')
                 ->type('description[1][title]', 'Seccion 2')
                 ->type('description[1][text]', 'Esta es la seccion 2')
                 ->press('Previsualizar');

             $this->assertDatabaseHas('versions', ['name' => 'Este es el título']);
             $this->assertDatabaseHas('versions_descriptions', ['title' => 'Seccion 2', 'text' => 'Esta es la seccion 2']);

             $browser->press('Atras')
                ->type('name', 'Edicion de Este es el título')
                ->type('description[1][title]', 'Edicion de Sección 2')
                ->press('Previsualizar');

            $this->assertDatabaseHas('versions', ['name' => 'Edicion de Este es el título']);
            $this->assertDatabaseHas('versions_descriptions', ['title' => 'Edicion de Sección 2']);
         });
    }   

}
