<?php

use App\User;


class PermisosMenuTest extends FeatureTestCase
{
    

    public function test_rol_usuario_ve_el_menu_correctamente()
    {       
        $user = factory(User::class)->create()->attachRole('user');

        $response = $this->actingAs($user)
                        ->visit('/')
                        ->assertResponseOk()
                        ->See('Fishbook')
                        ->dontSee('Login');
    }

    public function test_rol_admin_ve_el_menu_correctamente()
    {        
        $user = factory(User::class)->create()->attachRole('administrator');

        $response = $this->actingAs($user)
                                        ->visit('/')
                                        ->assertResponseOk()
                                        ->dontSee('Fishbook')
                                        ->dontSee('Login');
    }

}
