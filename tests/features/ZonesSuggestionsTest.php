<?php

use App\User;
use App\Version;
use App\Zone;


class ZonesSuggestionsTest extends FeatureTestCase
{
    public function test_user_can_propose_a_new_version(){
        $user = factory(User::class)->create()->attachRole('user');
        $zone = factory(Zone::class)->create();
        $version = factory(Version::class)->create(['validated' => true, 'published' => true, 'zone_id' => $zone->id]);

        $this->actingAs($user)
             ->visit(route('zones.show', [$version->id, $version->slug]))
             ->seeInElement($version->title, '<h1>' )
             ->click("Proponer un cambio / Añadir contenido")
             ->type('Este va a ser el nuevo nombre', 'name')
             ->press('Proponer');

        
        $this->seeInDatabase('versions', ['name' =>'Este va a ser el nuevo nombre', 'latitude' => $version->latitude, 'zone_id' => $zone->id]);
    }
}
