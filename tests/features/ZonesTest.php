<?php

use App\User;
use App\Version;
use App\Zone;


class ZonesTest extends FeatureTestCase
{
    
    public function test_user_cannot_see_his_zone_until_it_is_validated_and_published(){
        $user = factory(User::class)->create()->attachRole('user');
        $zone = factory(Zone::class)->create();
        $version = factory(Version::class)->create();

        $user->zones()->save($zone);
        $zone->versions()->save($version);
        $this->seeInDatabase('versions', ['name' => $version->name, 'validated' => false]);
        $this->get($version->url)->assertResponseStatus(404);

        $version->validated = true;
        $version->save();        
        $this->seeInDatabase('versions', ['name' => $version->name, 'validated' => true, 'published' => false]);
        $this->get($version->url)->assertResponseStatus(404);

        $version->published = true;
        $version->save();
        $this->seeInDatabase('versions', ['name' => $version->name, 'validated' => true, 'published' => true]);
        $this->get($version->url)->assertResponseStatus(200)->seeInElement('h1', $version->name);
    }

    public function test_user_cannot_publish_other_user_zone(){
        $user = factory(User::class)->create()->attachRole('user');
        $troll = factory(User::class)->create();
        $troll->attachRole('user');
        $zone = factory(Zone::class)->create();
        
        $user->zones()->save($zone);

        // When
        $this->actingAs($troll)
            ->visit(route('zones.publish', [$zone->id]))
            ->see('No esta autorizado');

    }

}
